The `sites_embed.html` code is intended to be copied and pasted into
an embedded HTML block in a search page on sites.wustl.edu.

This embeds an iframe. When the page is loaded, some JavaScript checks
for query parameters and uses them to set the URL on the iframe. 

Some more JavaScript listens for messages from the iframe that result
from interaction with the application, and updates the browser history
URLs accordingly. 