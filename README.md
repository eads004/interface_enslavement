This directory contains three coordinated subdirectories with code for
a new version of SLIDE search following data work in the 2022 HDW
summer workshop.

`/solr`: a schema and script for indexing CSV files into Solr.

`/server`: HTML/CSS/JavaScript with a minimally styled search
application that queries Solr. 

`/sites`: A bit of HTML for embedding JavaScript and an iframe in a
Sites page in order to embed the search application in SLIDE's Sites
page.