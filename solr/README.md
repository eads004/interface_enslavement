The configset directory has a Solr configset (created with
Solr-8.11.2) that includes a `managed-schema` file that can index SLIDE
dataset fields as of 29 August 2022.

The `index.sh` script runs in the same directory as the CSV files, on
the same machine that hosts Solr, and indexes the CSV files into a Solr
collection called `enslavementdb`.



