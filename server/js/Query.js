export {FACET_FIELDS, getQueryData}

const QUERYURLBASE = 'https://ada.artsci.wustl.edu/solr_enslavementdb/select';

const FACET_FIELDS = {
    'Recorded_status': 'Recorded Legal Status',
    'Sex': 'Sex',
    'Age_group': 'Age Range',
    'Occupation_type': 'Occupation Type',
    'Occupation_refined': 'Occupation',
    'Record_name': 'Record Name',
    'Record_year': 'Year',
    'County': 'County',
    'Township': 'Township',
    'Location': 'Location',
    'Last_first_middle_copy': 'Name'
};


function getQueryURL (state) {
    let params = new URLSearchParams();
    if ((!state.hasOwnProperty('term')) ||
          (state['term'] === '')) {
        params.append("q", "*:*");
    } else {
        params.append("q", `_text_:${state['term']}`);
    }
    if (state['start']) {
        params.append("start", state['start']);
    } else {
        params.append("start", "0");
    }
    if (state['recordid']) {
        params.delete("q");
        params.append("q", `Record_ID:${state['recordid']}`);
    }
    if (state['fq']) {
        for (let fieldname in state['fq']) {
            let value = state['fq'][fieldname];
            if (value.includes(' ')) {
                value = `"${value}"`;
            }
            params.append("fq", `${fieldname}:${value}`);
        }
    }
    params.append("facet", 'true')
    params.append("facet.sort", 'index')
    for (let field in FACET_FIELDS) {
        params.append("facet.field", field)
    }
    let queryurl = QUERYURLBASE + '?' + params.toString();
    return(queryurl);
};

function getQueryData (state, callback) {
    function requestLoaded () {
        let response = JSON.parse(this.responseText);
        callback(response);
    }
    const oReq = new XMLHttpRequest();
    oReq.addEventListener("load", requestLoaded);
    oReq.open("GET", getQueryURL(state));
    oReq.send();
};

