import {FACET_FIELDS} from './Query.js';
import {range} from './slideutil.js';
import {state, addFilter, removeFilter} from './State.js';

export {populateFacets};

function populateFacets (elementid) {
    console.log("STATE: ", state);
    let name_filter_is_on = 'Last_first_middle_copy' in state['fq'];
    let facet_data = state['response']['facet_counts']['facet_fields'];
    let elt = document.getElementById(elementid);
    elt.innerHTML = "";
    elt.appendChild(document.createElement("ul"));
    let elt_ul = document.getElementById(elementid)
        .getElementsByTagName("ul")[0];
    for (const facet_key in facet_data) {
        if (facet_key !== 'Last_first_middle_copy' || name_filter_is_on) {
            // ignore name as facet unless we're filtering
            let elt_li = document.createElement("li");
            elt_li.classList.add("hidden"); 
            elt_li.innerHTML = `<span class='fieldname' data-key='${facet_key}'>` + 
                `${FACET_FIELDS[facet_key]}</span>`;
            elt_ul.appendChild(elt_li);
            populateFacetValues(elt_li, facet_key, facet_data[facet_key], state);
        }
    }
    activateCheckboxes();
    makeCollapsible();
}

function populateFacetValues (elt_li, facet_key, facet_value_map) {
    let s_ul = document.createElement("ul");
    elt_li.appendChild(s_ul);
    range(0, facet_value_map.length - 1, 2).map(
        i => {
            const value = facet_value_map[i];
            const count = facet_value_map[i+1];
            let checked = false;
            if (facet_key in state['fq']) {
                if (value === state['fq'][facet_key]) {
                    checked = true;
                }
            }
//            if (facet_value_map[i+1] > 0) {
                let s_li = document.createElement('li');
                s_li.classList.add("valuecount");
                if (checked) {
                    s_li.classList.add("checked");
                }
                if (value === 'NULL') {
                    s_li.classList.add("null");
                }
                s_li.innerHTML = 
                    `<input type='checkbox' ${checked?'checked':''}></input> ` +
                    `<span><span class='value'>${value}</span> &nbsp;` + 
                    `<span class='count'>(${parseInt(count).toLocaleString()})` + 
                    `</span></span>  `;
                s_ul.appendChild(s_li);
//            }
        })
}

function activateCheckboxes() {
    let boxes = document.getElementById("facetbox")
        .getElementsByTagName("input");
    Object.values(boxes).map(
        box => {
            box.addEventListener(
                "change",
                e => {
                    const fieldname = box.parentNode.parentNode.parentNode
                          .getElementsByClassName('fieldname')[0]
                          .attributes['data-key'].textContent;
                    const value = box.parentNode
                        .getElementsByClassName('value')[0].textContent;
                    if (box.checked) {
                        addFilter(fieldname, value);
                    } else {
                        removeFilter(fieldname, value);
                    }})})};

function makeCollapsible() {
    const fieldnames = document.getElementById("facetbox")
        .getElementsByClassName("fieldname");
    Object.values(fieldnames).map(fieldname => fieldname.addEventListener(
        "click", 
        e => {
            const li = fieldname.parentNode 
            if (li.classList.contains("hidden")) {
                li.classList.remove("hidden") 
            } else {
                li.classList.add("hidden")
            }
        }));
}

