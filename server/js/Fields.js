export {FIELD_LABEL, FIELD_GROUPS, SUMMARY_FIELD_GROUPS};

const FIELD_LABEL = {
    'Record_ID': 'Record ID',
    'Record_name': 'Record Name',
    'Name_standardized': 'Name (Standardized)',
    'Name_original': 'Name',
    'Enslaved_person.name.original': 'Name',
    'Last_first_middle_copy': 'Name',
    'Alternate_name(s)': 'Alternate Name(s)',
    'Recorded_status': 'Recorded Status',
    'Sex': 'Sex',
    'Birth_date': 'Birth Date',
    'Display_age': 'Age',
    'Age_group': 'Age Range',
    'Height': 'Height',
    'Skin_tone_indicator': 'Indicator of Skin Tone',
    'Physical_description': 'Physical Description',
    'Occupation_type': 'Occupation Type',
    'Enslaved_person.occupation_type': 'Occupation Type',
    'Occupation_original': 'Occupation (original)',
    'Occupation_refined': 'Occupation (refined)',
    'Geographic_ties': 'Geographic Ties',
    'Relationships': 'Relationships',
    'Enslaved_person.relationships': 'Relationships',
    'Place_name': 'Scheduled Place of Sale',
    'Sale_date': 'Scheduled Date of Sale',
    'Sale_time': 'Scheduled Sale Time',
    'Dollar_price_of_sale': 'Appraised Price',
    'Total_sum_if_sold_together': 'Total Appraised Price for a Group',
    'Bond_amount': 'Bond Amount',
    'Punishment': 'Punishment',
    'Enslaver_full_original': 'Enslaver (original)',
    'Enslaver_full_standardized': 'Enslaver',
    'Emancipator_full_original': 'Emancipator (original)',
    'Emancipator_full_standardized': 'Emancipator',
    'Emancipator_description': 'Emancipator Description',
    'Security_full_original': 'Security (original)',
    'Security_full_standardized': 'Security',
    'Estate_full_original': 'Estat (original)',
    'Estate_full_standardized': 'Estate',
    'Administrator_full_original': 'Estate (original)',
    'Administrator_full_standardized': 'Administrator',
    'Buyer_full_original': 'Buyer (original)',
    'Buyer_full_standardized': 'Buyer',
    'Heir_full_original': 'Heir (original)',
    'Heir_full_standardized': 'Heir',
    'Additional_information': 'Additional Information',
    'Transcription': 'Transcription',
    'Partial_transcription': 'Partial Transcription',
    'Record_type': 'Record Type',
    'Record_date': 'Record Date',
    'Record_year': 'Record Year',
    'County': 'County',
    'Township': 'Township', 
    'Location': 'Location',
    'Volume': 'Volume',
    'Page_number': 'Page Number',
    'Cross_referenced': 'Cross-referenced',
    'Link_to_source': 'Link to Source',
    'Standardization_decision': 'Standardization decision',
    'Developer_notes': 'Developer Notes',
    'Contributor': 'Contributor',
    'Enslaved_person.name.alternate': 'Alternate Name',
    'Enslaved_person.sex': 'Sex',
    'Enslaved_person.sex.inferred_recorded': 'Sex inferred?',
    'Birth_date': 'Birth Date',
    'Enslaved_person.birth_date': 'Birth Date',
    'Skin_tone_indicator': 'Racial Description',
    'Enslaved_person.skin_tone_indicator': 'Racial Description',
    'Enslaved_person.height': 'Height',
    'Enslaved_person.approximate_weight': 'Approximate weight',
    'Physical_description': 'Physical Description',
    'Enslaved_person.ethnic_description': 'Ethnic Description',
    'Enslaved_person.injuries_scars': 'Injuries/scars',
    'Enslaved_person.other_physical_descriptions': 'Physical Description',
    'Enslaved_person.possesions': 'Possessions',
    'Enslaved_person.literacy': 'Literacy',
    'Enslaved_person.language': 'Language',
    'Escape.type': 'Escape Type',
    'Escape.date.day': 'Escape Day',
    'Escape.date': 'Escape Date',
    'Reward_offer': 'Reward Offer',
    'Reward_criteria': 'Reward Criteria',
    'Advertisement.language': 'Advertisement Language',
    'Newspaper.location.township': 'Newspaper Location Township', 
    'Newspaper.location.city': 'Newspaper Location City', 
    'Newspaper.location.county': 'Newspaper Location County', 
    'Newspaper.location.state': 'Newspaper Location State', 
    'Newspaper.location.country': 'Newspaper Location Country', 
    'Enslaver.gender': 'Enslaver gender',
    'Enslaver.gender.inferred_recorded': 'Enslaver gender inferred?',
    'Enslaver.type': 'Enslaver type',
    'Enslaver.location.name': 'Enslaver location',
    'Advertiser.name': 'Advertiser',
    'Advertiser.type': 'Advertiser type',
    'Advertiser.location': 'Advertiser location',
    'Advertiser.location.city': 'Advertiser city',
    'Advertiser.location.county': 'Advertiser county',
    'Advertiser.location.state': 'Advertiser state',
    'Advertiser.location.country': 'Advertiser country',
    'Enslaved_person.geographic_ties': 'Geographic Ties',
    'Record_publication_date': 'Publication Date',
    'Day_of_ad_submission': 'Day of Ad Submission'


}


const SUMMARY_FIELD_GROUPS = {
    'name': {
        'Name_standardized': 'Name (Standardized)',
	'Enslaved_person.name.original': 'Name',
    },
    'personal_information': {
        'Sex': 'Sex',
        'Display_age': 'Age',
        'Height': 'Height'
    },
    'status': {
        'Recorded_status': 'Recorded Status',
    },
    'record_information': {
        'Record_name': 'Record Name',
        'Record_date': 'Record Date',
        'Record_ID': 'Record ID'
    },
    'other_information': {
        'Occupation_refined': 'Occupation (refined)',
        'Enslaver_full_standardized': 'Enslaver (Standardized Name)',
        'Emancipator_full_standardized': 'Emancipator (Standardized Name)',
    }
}

const FIELD_GROUPS = {
    'name': {
        'Name_standardized': 'Name (Standardized)',
        'Name_original': 'Name',
	'Enslaved_person.name.original': 'Name'
    },

    'status': {
        'Recorded_status': 'Recorded Status',
    },

    'contributor': {
        'Contributor': 'Contributor'
    }, 

    'location_details': {
        'County': 'County',
        'Township': 'Township', 
        'Location': 'Location',
    },

    'personal_info_1': {
        'Sex': 'Sex',
        'Display_age': 'Age',
        'Height': 'Height',
    },

    'personal_info_2': {
        'Alternate_name(s)': 'Alternate Name(s)',
        'Enslaved_person.name.alternate': 'Alternate Name',
        'Enslaved_person.sex': 'Sex',
        'Enslaved_person.sex.inferred_recorded': 'Sex inferred/recorded?',
        'Birth_date': 'Birth Date',
        'Enslaved_person.birth_date': 'Birth Date',
        'Skin_tone_indicator': 'Racial Description',
        'Enslaved_person.skin_tone_indicator': 'Racial Description',
        'Enslaved_person.height': 'Height',
        'Enslaved_person.approximate_weight': 'Approximate weight',
        'Physical_description': 'Physical Description',
        'Enslaved_person.ethnic_description': 'Ethnic Description',
        'Enslaved_person.injuries_scars': 'Injuries/scars',
        'Enslaved_person.other_physical_descriptions': 'Physical Description',
        'Enslaved_person.possesions': 'Possessions',
        'Enslaved_person.literacy': 'Literacy',
        'Enslaved_person.language': 'Language'
    },

    'additional_info': {
        'Additional_information': 'Additional Information'
    }, 

    'occupation_information': {
        'Occupation_type': 'Occupation Type',
        'Enslaved_person.occupation_type': 'Occupation Type',
        'Occupation_original': 'Occupation (original)',
        'Occupation_refined': 'Occupation (refined)'
    },

    'other_information': {
        'Geographic_ties': 'Geographic Ties',
        'Enslaved_person.geographic_ties': 'Geographic Ties',
        'Relationships': 'Relationships',
        'Enslaved_person.relationships': 'Relationships',
        'Partial_transcription': 'Partial Transcription',
        'Transcription': 'Transcription'
    },

    'notes': {
        'Developer_notes': 'Developer Notes',
        'Standardization_decision': 'Standardization decision'
    },

    'record_information': {
        'Record_name': 'Record Name',
        'Record_date': 'Record Date',
        'Record_ID': 'Record ID'
    },
  
    'record_details_1': {
        'Record_type': 'Record Type',
        'Volume': 'Volume',
        'Page_number': 'Page Number',
    },

    'record_details_2': {
	'Record_publication_date': 'Publication Date',
	'Day_of_ad_submission': 'Day of Ad Submission',
        'Place_name': 'Scheduled Place of Sale',
        'Sale_date': 'Scheduled Date of Sale',
        'Sale_time': 'Scheduled Sale Time',
        'Dollar_price_of_sale': 'Appraised Price',
        'Total_sum_if_sold_together': 'Total Appraised Price for a Group',
        'Bond_amount': 'Bond Amount',
        'Punishment': 'Punishment',
        'Link_to_source': 'Link to Source',
        'Cross_referenced': 'Cross-referenced',
	'Escape.type': 'Escape Type',
	'Escape.date.day': 'Escape Day',
	'Escape.date': 'Escape Date',
	'Reward_offer': 'Reward Offer',
	'Reward_criteria': 'Reward Criteria',
	'Advertisement.language': 'Advertisement Language',
	'Newspaper.location.township': 'Newspaper Location Township', 
	'Newspaper.location.city': 'Newspaper Location City', 
	'Newspaper.location.county': 'Newspaper Location County', 
	'Newspaper.location.state': 'Newspaper Location State', 
	'Newspaper.location.country': 'Newspaper Location Country', 
    },

    'other_people_fields': {
        'Enslaver_full_standardized': 'Enslaver',
        'Enslaver.gender': 'Enslaver gender',
        'Enslaver.gender.inferred_recorded': 'Enslaver gender inferred/recorded?',
        'Enslaver.type': 'Enslaver type',
        'Enslaver.location.name': 'Enslaver location',
        'Emancipator_full_standardized': 'Emancipator',
        'Security_full_standardized': 'Security',
        'Estate_full_standardized': 'Estate',
        'Administrator_full_standardized': 'Administrator',
        'Buyer_full_standardized': 'Buyer',
        'Heir_full_standardized': 'Heir',
	'Advertiser.name': 'Advertiser',
	'Advertiser.type': 'Advertiser type',
	'Advertiser.location': 'Advertiser location',
	'Advertiser.location.city': 'Advertiser city',
	'Advertiser.location.county': 'Advertiser county',
	'Advertiser.location.state': 'Advertiser state',
	'Advertiser.location.country': 'Advertiser country',
	
    },

    'other_people_fields_original': {
        'Enslaver_full_original': 'Enslaver (original)',
        'Emancipator_full_original': 'Emancipator (original)',
        'Emancipator_description': 'Emancipator Description',
        'Security_full_original': 'Security (original)',
        'Estate_full_original': 'Estate (original)',
        'Administrator_full_original': 'Administrator (original)',
        'Buyer_full_original': 'Buyer (original)',
        'Heir_full_original': 'Heir (original)',
    }
}
