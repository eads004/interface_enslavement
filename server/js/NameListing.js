import {range} from './slideutil.js';
import {state} from './State.js';

export {populateNameList};


function isCapitalAlpha(letter) {
    return letter >= 'A' && letter <= 'Z';
};

function populateAlphaLinks(letters) {
    let elt = document.getElementById("alpha_links");
    letters.forEach(letter => {
        let a = document.createElement("a");
        a.classList.add("alpha_link");
        a.text = letter;
        elt.appendChild(a);
    });
};

function activateAlphaLinks() {
    let letters = document.getElementsByClassName("alpha_link");
    Array.from(letters).forEach(letter => {
        letter.addEventListener("click", elt => {
            let name = elt.target.text;
            let heading = document.getElementsByName(name)[0];
            heading.scrollIntoView({block: 'center', behavior: 'smooth'});
        })
    })}
    ;

function populateNameList (elementid) {
    let name_pairs = state['nameresponse']['facet_counts']['facet_fields']['Last_first_middle_copy'];
    let first_letters = new Set();
    for (let i = 0; i < name_pairs.length - 1; i += 2) {
        let letter = name_pairs[i][0];
        if (isCapitalAlpha(letter)) {
            first_letters.add(letter);
        }
    };
    populateAlphaLinks(first_letters);
    let elt = document.getElementById(elementid);
    elt.innerHTML = "";
    elt.appendChild(document.createElement("ul"));
    let elt_ul = document.getElementById(elementid)
        .getElementsByTagName("ul")[0];

    let prev_letter = "";

    for (let i = 0; i < name_pairs.length - 1; i += 2) {
            const value = name_pairs[i];
            const count = name_pairs[i+1];
            if (value[0] != prev_letter && isCapitalAlpha(value[0])) {
                prev_letter = value[0];
                let anchor = document.createElement("a");
                let name = document.createAttribute("name");
                anchor.setAttribute("name", prev_letter);
                anchor.text = prev_letter;
                elt_ul.appendChild(anchor);
            }
            if (name_pairs[i+1] > 0) {
                let s_li = document.createElement('li');
                s_li.classList.add("valuecount");
                s_li.innerHTML = 
                    `<span><span class='namevalue'>` + 
                     `${value}</span> &nbsp;` + 
                    `<span class='count'>(${parseInt(count).toLocaleString()})` + 
                    `</span></span>  `;
                elt_ul.appendChild(s_li);
            }
    };
    range(0, name_pairs.length - 1, 2).map(
        i => {
        })
    activateNameLinks(elementid);
    activateAlphaLinks();
}



function activateNameLinks (elementid) {
    let namelinks = document.getElementById(elementid)
        .getElementsByClassName("namevalue");
    Object.values(namelinks).map(
        namelink => {
            namelink.addEventListener(
                "click",
                e => {
                    let value = namelink.textContent;
                    window.top.postMessage({'name': value}, "*");
                }
            );
        });
    window.onmessage = function(event){
        if ('name' in event.data) {
            const frame = document.getElementById("searchframe");
            const name = event.data['name'];
            let params = new URLSearchParams();
            // params.append('term', `"${name}"`);
            params.append("fq", `Last_first_middle_copy:${name}`);
            const newframeurl = "index.html?" + params.toString();
            window.location.href = newframeurl;
        }
    };
    
}
