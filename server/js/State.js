import {getQueryData} from './Query.js';
import {populateFacets} from './Facets.js';
import {populateResults} from './Results.js';

export {state, setStateFromParams, setStart, addFilter, removeFilter,
       setSearchTerm};

let state = {};

function setStateFromParams(params) {
    let term = params.get("term") || "";
    state['term'] = term;
    state['start'] = parseInt(params.get("start")) || 0;
    let fqs = params.getAll("fq");
    if (fqs) {
        state['fq'] = {};
        fqs.forEach(v => {
            let [fieldname, value] = v.split(":");
            value = value.replace(/^"(.*)"$/, "$1");
            state['fq'][fieldname] = value;
        })
    }
}

function setURLFromState() {
    let params = new URLSearchParams(window.location.search);
    params.delete("start");
    params.delete("term");
    params.delete("fq");
    if (state['start']) {
        params.set("start", state['start']);
    };
    if (state['term'] !== '') {
        params.set("term", state['term']);
    };
    for (var key in state['fq']) {
        let value = state['fq'][key];
        params.append("fq", `${key}:${value}`);
    }
    let url = window.location.href.replace(window.location.search, '')
    .replace(/\?$/, '');
    if (params.toString() !== '') {
        url += "?" + params.toString()
    };
    // send message to parent of iframe:
    let pstring = params.toString();
    window.top.postMessage(pstring, "*")
    history.pushState({}, '', url);
}

function setStart(newstart) {
    if (newstart !== state['start']) {
        state['start'] = newstart;
        getQueryData(state, v => {
            state['response'] = v;
            setURLFromState();
            populateResults(state);
        })
    };
}

function populateFromState() {
    getQueryData(state, v => {
        state['response'] = v;
        setURLFromState();
        populateFacets("facetbox", state);
        populateResults(state);
    })
}

function addFilter (fieldname, value) {
    state['fq'][fieldname] = value;
    state['start'] = 0;
    populateFromState();
}

function removeFilter (fieldname, value) {
    delete(state['fq'][fieldname]);
    state['start'] = 0;
    populateFromState();
}

function setSearchTerm (term) {
    state['term'] = term;
    state['start'] = 0;
    populateFromState();
}
