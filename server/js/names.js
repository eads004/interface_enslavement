import {getNameQueryData} from './NameQuery.js';
import {state, setStateFromParams} from './State.js';
import {populateNameList} from './NameListing.js';


getNameQueryData(state, v => {
    state['nameresponse'] = v;
    populateNameList("name_list");
});

