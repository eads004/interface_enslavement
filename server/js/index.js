import {getParams, setURL} from './Route.js';
import {state, setStateFromParams} from './State.js';
import {getQueryData} from './Query.js';
import {populateFacets} from './Facets.js';
import {populateResults} from './Results.js';
import {activateSearchbox} from './Search.js';

let params = getParams();

setStateFromParams(params);

getQueryData(state, v => {
    state['response'] = v;
    populateFacets("facetbox");
    populateResults();
    activateSearchbox();
});




