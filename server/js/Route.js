export {getParams, setURL};

function getParams() {
    let params = new URLSearchParams(window.location.search);
    return params;
}

function setURL(params) {
    const url = new URL(window.location);
    for (const p of params) {
        if (url.searchParams.has(p[0])) {
            url.searchParams.delete(p[0]);
        }
        url.searchParams.append(p[0], p[1]);
    }
    window.history.pushState({}, '', url.toString());
}
