export {populateResults};
import {range} from './slideutil.js';
import {state, setStart, setSearchTerm, removeFilter} from './State.js';
import {FIELD_LABEL, SUMMARY_FIELD_GROUPS} from './Fields.js';

function showQueryTerms (state) {
    let resultMsg = "";
    if (state['term'] !== '') {
        resultMsg += `<span class='queryterm'>Search term: ${state['term']}</span>`;
    };
    for (let i in Object.keys(state['fq'])) {
        let key = Object.keys(state['fq'])[i];
        resultMsg += `<span class='queryfq' ` +
            `data-fieldname='${key}' data-value='${state['fq'][key]}'>${FIELD_LABEL[key]}: ${state['fq'][key]}</span>`;
    };
    return resultMsg;
}

function activateQueryTerms () {
    let termelt = document.getElementsByClassName("queryterm")[0];
    if (termelt) {
        termelt.addEventListener("click", e => {
            document.getElementById("searchbox").value = "";
            setSearchTerm("");
        });
    };
    let queryfqs = Object.entries(
        document.getElementsByClassName("queryfq"));
    queryfqs.map(v => {
        v[1].addEventListener("click", e => {
            let fieldname = v[1].attributes['data-fieldname'].value;
            let fieldvalue = v[1].attributes['data-value'].value;
            removeFilter(fieldname, fieldvalue);
        })
    });
 }

function populateResults () {
    const count = state['response']['response']['numFound'];
    const start = state['start'] + 1;
    const resultMsg = `<div>${start.toLocaleString()} to ` + 
          `${(Math.min(start + 9, count)).toLocaleString()} ` + 
          `of ${count.toLocaleString()} results</div>`;
    document.getElementById("resultcount").innerHTML = resultMsg;
    const queryterms = showQueryTerms(state);
    document.getElementById("querytermblock").innerHTML = queryterms;
    activateQueryTerms();
    setPagination(count, 'pagination_top');
    setPagination(count, 'pagination_bottom', false);
    populateResultList(state);
}

function setPagination(count, eltid, shorter=true) {
    const pagination = document.getElementById(eltid);
    const pagecount = 1 + Math.floor((count - 1)/ 10);
    const currentpage = 1 + Math.floor(state['start']/ 10);
    const minpage = Math.max(1, 
                             Math.min(Math.max(currentpage - 3, 1), 
                                      pagecount - 6));
    const maxpage = Math.min(pagecount, 
                             Math.max(
                                 Math.min(currentpage + 3, pagecount), 
                                 7));
    let reset = document.createElement("span");
    reset.textContent = "ᐸᐸ";
    if (currentpage === 1) {
        reset.classList.add("inactive");
    } else {
        reset.classList.remove("inactive");
    }
    reset.addEventListener("click", e => setStart(0));
    pagination.innerHTML = "";
    pagination.appendChild(reset);
    let back = document.createElement("span");
    back.textContent = " ᐸ ";
    if (currentpage === 1) {
        back.classList.add("inactive");
    } else {
        back.classList.remove("inactive");
    }
    back.addEventListener("click", e => 
                          setStart(Math.max(0, (currentpage - 2) * 10)));
    pagination.appendChild(back);

    if (shorter) {
        let thispage = document.createElement("span");
        thispage.textContent = `${currentpage}`;
        thispage.classList.add("currentpage");
        pagination.appendChild(thispage);
    } else {
        range(minpage, maxpage, 1).map(i => {
            let span = document.createElement("span");
            if (i === currentpage) {
                span.classList.add("current");
            };
            span.textContent = ` ${i} `;
            span.addEventListener("click", e => setStart((i - 1) * 10));
            pagination.appendChild(span);
        });
    }
    let forward = document.createElement("span");
    forward.textContent = " ᐳ ";
    if (currentpage === pagecount) {
        forward.classList.add("inactive");
    } else {
        forward.classList.remove("inactive");
    }
    forward.addEventListener("click", e => 
                          setStart(Math.min((pagecount - 1) * 10, 
                                            currentpage * 10)));
    pagination.appendChild(forward);
    let toend = document.createElement("span");
    toend.textContent = " ᐳᐳ ";
    if (currentpage === pagecount) {
        toend.classList.add("inactive");
    } else {
        toend.classList.remove("inactive");
    }
    toend.addEventListener("click", e => setStart((pagecount - 1) * 10));
    pagination.appendChild(toend);
}

function populateResultList() {
    let resultlist = document.getElementById("resultlist");
    resultlist.innerHTML = "";
    let docs = state['response']['response']['docs'];
    docs.forEach(doc => {
        let container = document.createElement("div");
        container.classList.add("record");
        let handle = document.createElement("div");
        handle.classList.add("handle");
        container.appendChild(handle);
        Object.entries(SUMMARY_FIELD_GROUPS).map(([grouplabel, group]) => {
            let ul = document.createElement("ul");
            ul.classList.add(grouplabel);
            let section_start = true;
            Object.entries(group).map(([field, label]) => {
                if (field in doc) {
                    let value = doc[field];
                    let nullclass = (value==='NULL' || value[0]==='NULL') ? ' null' : '';
                    let row = document.createElement("li");
                    row.classList.add(field);
                    if (nullclass !== '') {
                        row.classList.add("null");
                    }
                    if (section_start) {
                        row.classList.add("section_start");
                        section_start = false;
                    }
                    row.innerHTML = `<span class='field'>${FIELD_LABEL[field]}: </span> ` + 
                        `<span class='value ${nullclass}'>${value}</span> `;
                    ul.appendChild(row);
                }
            })
            container.appendChild(ul)
        })
        let link = document.createElement("a");
        link.href = `record.html?recordid=${doc['Record_ID']}`;

        let button = document.createElement("button");
        button.classList.add("addbutton");

        link.appendChild(button);
        container.appendChild(link);

        resultlist.appendChild(container);
    });
}

