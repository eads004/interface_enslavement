export {getNameQueryData}

const NAMEQUERYURL = 'https://ada.artsci.wustl.edu/solr_enslavementdb/select?facet.field=Last_first_middle_copy&facet.limit=10000&facet.sort=index&facet=true&indent=true&q.op=OR&q=*%3A*&rows=0';


function getNameQueryData (state, callback) {
    function requestLoaded () {
        let response = JSON.parse(this.responseText);
        callback(response);
    }
    const oReq = new XMLHttpRequest();
    oReq.addEventListener("load", requestLoaded);
    oReq.open("GET", NAMEQUERYURL);
    oReq.send();
};



