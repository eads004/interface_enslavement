import {getParams} from './Route.js';
import {getQueryData} from './Query.js';
import {FIELD_LABEL, FIELD_GROUPS} from './Fields.js';

let params = getParams();
let state = {};

let recordid = params.get("recordid") || "";

function make_std_name_popup(doc, stdnamefield) {
    let orignamefield = stdnamefield.replace('standardized','original');
    if (doc[orignamefield] !== 'NULL' && doc[orignamefield][0] !== doc[stdnamefield][0]) {
        let stdname = doc[stdnamefield][0];
        let pair = document.createElement("span");
        pair.classList.add("original_popup");
    
        let icon = document.createElement("div");
        icon.classList.add("icon");
        let img = document.createElement("img");
        img.src = "images/icard.png";
        icon.appendChild(img);
        

        let popup_info = document.createElement("div");
        popup_info.classList.add("popup_info");
        

        let label = document.createElement("span");
        label.classList.add("label");
        label.textContent = 'original: ';
        
      
        let value = document.createElement("span");
        value.classList.add("value");
        value.textContent = doc[orignamefield][0];
        

        popup_info.appendChild(label);
        popup_info.appendChild(value);
        

        pair.appendChild(icon);
        pair.appendChild(popup_info);
        
        return pair;
    } 
    else {
        return "";
    }
}


if (recordid === "") {
    window.location="index.html";
} else {
    getQueryData({'recordid': recordid}, v => {
        let result = v['response']['docs'];
        if (result.length === 0) {
            document.getElementById("record").innerHTML("<h2>No record found with ID " + recordid);
        } else {
            let doc = result[0];
            Object.entries(FIELD_GROUPS).map(([grouplabel, group]) => {
                let container = document.getElementById(grouplabel);
                Object.entries(group).map(([field, label]) => {
                    if (field in doc) {
                        let value = doc[field];
                        if (value !== 'NULL' && value[0] !== 'NULL') {
                            let row = document.createElement("li");
                            row.classList.add(field);
                            if (field === 'Link_to_source') {
                                row.innerHTML = `<span class='field'>Source: </span> ` + 
                                    `<span class='value'><a href='${value}'>[Link to source]</a></span> `;
                            } else {
                                row.innerHTML = `<span class='field'>${FIELD_LABEL[field]}: </span> ` + 
                                    `<span class='value'>${value}</span> ` ;
                                if (field.includes('standardized')) {
                                    let popup = make_std_name_popup(doc, field);
                                    row.append(popup);
                                };

                            };
                            container.appendChild(row);
                        }
                    }
                })
            })
        }
    });
};




