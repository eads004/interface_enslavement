export {activateSearchbox};

import {setSearchTerm, state} from './State.js';


function activateSearchbox() {
    let searchbox = document.getElementById("searchbox");
    if (state['term']) {
        searchbox.value = state['term'];
    }
    searchbox.addEventListener("change", e => {
        setSearchTerm(e.target.value);
    });
};
