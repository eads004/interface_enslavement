This code goes in the web root of the embedded iframe application. It
works as a standalone application without special page styling,
explanation, or navigation.

It's a plain JavaScript application without dependencies, split into
small modules to try to gain a little clarity and separation of
concerns, where functions or objects don't need to be exported to other contexts. 

The State module and `state` object are threaded through almost
everything. Other modules read the state object, but the State module
itself is intended to be the sole site of code that decides when the
URL needs to be updated as the result of interaction, or when
interface elements need to be updated because of the URL.
